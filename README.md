# Weui Dynamic Form

[中文](#中文)

[US](#en-US)

## 中文

这是一个由 [cy948](https://github.com/cy948)创建的微信小程序原生表单的练习，配合视频和博客享用更佳：

- B站视频: https://www.bilibili.com/video/BV1oQ4y1m7it/
- 微信开放社区文章: https://developers.weixin.qq.com/community/develop/article/doc/000c82fa6a073870d30d5db9551013

### 快速开始



## en-US

This project is created by [cy948](https://github.com/cy948) as a tutorial. Video and blog can be found here:

- Video on Bilibili.com: https://www.bilibili.com/video/BV1oQ4y1m7it/
- Blog on developers.weixin.qq.com: https://developers.weixin.qq.com/community/develop/article/doc/000c82fa6a073870d30d5db9551013

### QuickStart

#### Import

Ensure you have a recent version of [Weixin DevTools](https://developers.weixin.qq.com/miniprogram/en/dev/devtools/stable.html) installed.



