const formConfig = {
    title: "我的表单",
    desc: "这是一个工单，填就完事了",
    rules: [
    {
        name: 'ticketClass',
        rules: {required: true, message: '分类多少选一个吧'},
    }, {
        name: 'problems',
        rules: {required: true, message: '这么多你一个都不选？'},
    }, {
        name: 'mobile',
        rules: [
            {required: true, message: '手机号别忘啊，忘了我怎么找你'}, {mobile: true, message: 'mobile格式不对'}
        ],
    },{
        name: 'username',
        rules: {required: true, message: '名字必填'},
    },{
        name: 'userbuilding',
        rules: [
            {required: true, message: '楼栋没填'}
        ],
    },{
        name: 'userroomnum',
        rules: [
            {required: true, message: '房号不能为空'},
            {range: [100,750], message: '哪有这房间？'}
        ],
    },{
        name: 'userfreetime',
        rules: {required: true, message: '请填写空闲时间'},
    }
    ],
    formInfo: [
        {
            id: 'ticketClass',
            title: '问题分类',
            type: 'checkbox',
            settings: {
                multi: false,
                prop: 'radio',
                options: [
                    {name: '财务类', value: '0', checked: true},
                    {name: '硬件故障', value: '1'},
                    {name: '软件故障', value: '2'}
                ]
            }
        },
        {
            id: 'problems',
            title: '常见部件问题',
            type: 'checkbox',
            settings: {
                multi: true,
                prop: 'radio',
                options: [
                    {name: '网线接口烂了', value: '0'},
                    {name: '上不了网', value: '1'},
                    {name: '能连上内网但上不了网', value: '2'},
                    {name: '能连上但好慢', value: '2'}
                ]
            }
        },
        {
            id: 'username',
            title: '',
            type: 'input',
            settings: {
                title: '姓名',
                prop: '',
                dataField: '',
                placeHolder:'请填写姓名',
                options: [

                ]
            }
        },
        {
            id: 'userbuilding',
            title: '',
            type: 'picker-input',
            settings: {
                pickerId:'userroomnum',
                title: '你的房号',
                prop: '',
                dataField: '',
                placeHolder:'请填写你的房号',
                pickerIndex: 0,
                range: ["8栋", "9栋", "10栋", "11栋"],
                placeholder: '请输入房号',
                options: [

                ]
            }
        },
        {
            id: 'userfreetime',
            title: '空闲时间',
            type: 'date',
            settings: {
                mode: 'date',
                title: '点此选择',
                prop: 'date',
                dataField: 'date',
                start: '2021-11-01',
                end:'2022-01-01',
                placeHolder:'请填写空闲时间',
                options: [

                ]
            }
        },
        {
            id: 'usermore',
            title: '备注',
            type: 'textarea',
            settings: {
                title: '',
                prop: 'date',
                dataField: '',
                placeHolder:'请填写备注',
                warn: '爱写不写',
                options: [

                ]
            }
        },
        {
            id: 'userpics',
            title: '图片上传',
            type: 'uploader',
            settings: {
                title: '',
                prop: 'date',
                dataField: '',
                placeHolder:'请填写备注',
                tips: '图片上传提示',
                maxcount: 5,
                basePath: 'form/uploads/',
                options: [

                ]
            }
        },
        {
            id: 'vcode',
            title: '验证码',
            type: 'vcode',
            settings: {
                title: '',
                prop: 'vcode',
                dataField: 'vcodeNum',
                placeHolder:'请填写验证码',
                vcodeVerifyCode:'vcodeVerifyCode', //加盐后的验证码的字段名称
                warn: '验证码',
                options: [

                ]
            }
        },
        {
            id: 'mobile',
            title: '手机',
            type: 'mobile',
            settings: {
                title: '',
                prop: 'mobile',
                dataField: 'mobile',
                placeHolder:'请填写验证码',
                vcodeVerifyCode:'vcodeVerifyCode', //加盐后的验证码的字段名称
                warn: '验证码',
                options: [

                ]
            }
        },
    ],
    agreeLink: "https://developers.weixin.qq.com/miniprogram/dev/framework/"

}
export default formConfig;